using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Net.Sockets;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class MoleMovement : MonoBehaviour
{
    [SerializeField] private float risingSpeed = 2f;
    [SerializeField] private float walkingSpeed = 2f;
    [SerializeField] private float stoppingDistance = 1.5f;
    
    private Transform playerTransform;
    private Rigidbody rigidBody;

    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    [SerializeField] private ParticleSystem dirtMound;
    
    private void Start()
    {
        // initially mole is below ground
        // where he has no gravity and can pass through ground
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;
        rigidBody.isKinematic = true;
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
    }
    private void Awake()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        
        Vector3 dirtPosition = new Vector3(
            transform.position.x, 
            GameObject.FindWithTag("ground").GetComponent<Collider>().bounds.max.y + 1f, 
            0f);
        Instantiate(dirtMound, dirtPosition, Quaternion.identity);
    }
    
    private void FixedUpdate()
    {
        // keep mole on z-plane 0
        Vector3 pos = transform.position;
        transform.position = new Vector3(pos.x, pos.y, 0);
        
        LookAtPlayer();
        if (!IsOnGround())
        {
            rigidBody.detectCollisions = false;
            Rise();
        }
        else
        {
            // mole is now tied by gravity again and cannot pass through objects
            rigidBody.detectCollisions = true;
            rigidBody.useGravity = true;
            rigidBody.isKinematic = false;

            // move towards player until near enough
            if (Vector3.Distance(transform.position, playerTransform.position) > stoppingDistance)
                MoveTowardsPlayer();
        }
    }

    void Rise()
    {
        // move above ground
        // TODO: instead of 1f use generic position (top of ground + enemy height)
        Vector3 curPos = transform.position;
        transform.position = Vector3.MoveTowards(curPos,
            new Vector3(curPos.x, GameObject.FindWithTag("ground").GetComponent<Collider>().bounds.max.y + 1f, curPos.z),
            Time.deltaTime * risingSpeed);
    }

    void MoveTowardsPlayer()
    {
        // move towards player in x direction
        Vector3 curPos = transform.position;
        transform.position = Vector3.MoveTowards(curPos, 
            new Vector3(playerTransform.position.x, curPos.y, 0), 
            Time.deltaTime * walkingSpeed);
    }

    bool IsOnGround()
    {
        return Physics.CheckSphere(groundCheck.position, 0.01f, groundLayer);
    }

    void LookAtPlayer()
    {
        // look at player but ignore y direction
        // otherwise this leads to weird rotation of the enemy
        Vector3 lookAtPosition = playerTransform.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);
    }
}

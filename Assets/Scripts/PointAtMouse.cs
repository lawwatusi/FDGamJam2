using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAtMouse : MonoBehaviour
{
    Vector3 mousePos;
    public Transform target; //Assign to the object you want to rotate
    Vector3 objectPos;
    float angle;
 
 void Update()
    {
        mousePos = Input.mousePosition;
        mousePos.z = 100f; //The distance between the camera and object
        objectPos = Camera.main.WorldToScreenPoint(target.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;
        angle = Mathf.Atan2(mousePos.y , mousePos.x ) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }
}

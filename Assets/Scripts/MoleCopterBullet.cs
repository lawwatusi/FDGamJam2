using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleCopterBullet : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float damage = 5f;
    
    public void ShootBullet()
    {
        Debug.DrawRay(transform.position, transform.forward, Color.cyan, 30f);
        GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed, ForceMode.Impulse);
        // Debug.Log(direction);
        // Debug.DrawRay(transform.position, direction, Color.cyan, 30f);
        // rigidBody.velocity = direction * moveSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            playerHealth.AddDamage(damage);
            playerHealth.damageFX(other.ClosestPointOnBounds(transform.position), -GetComponent<Rigidbody>().velocity);
            Debug.Log("Player health: " + playerHealth.currentHealth);
        }
    }
}

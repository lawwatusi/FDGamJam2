using System;
using System.Threading;
using UnityEngine;
using Random = UnityEngine.Random;


public class MoleCopterMovement : MonoBehaviour
{
    [SerializeField] private float flyingSpeed = 1.5f;
    
    private Transform playerTransform;
    private Bounds flyingArea;
    private Vector3 currentWayPoint;
    
    [SerializeField] private float speed = 0.1f;
    private Quaternion from;
    private Quaternion to;
    private float turnTime = 0f;
    private bool isFacingRight;

    private float nextStopTime, nextStopDuration, stopDuration;
    private bool isStopping;

    private MoleCopterShooter moleCopterShooter;

    [SerializeField] private float minTimeBetweenStops = 5f;
    [SerializeField] private float maxTimeBetweenStops = 15f;
    
    [SerializeField] private float minStopTime = 1f;
    [SerializeField] private float maxStopTime = 5f;

    private void Awake()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        moleCopterShooter = gameObject.GetComponent<MoleCopterShooter>();
        flyingArea = GameObject.FindWithTag("FlyingArea").GetComponent<Collider>().bounds;
        SetNextWayPoint();
        nextStopTime = Random.Range(minTimeBetweenStops, maxTimeBetweenStops);
        nextStopDuration = Random.Range(minStopTime, maxStopTime);
    }

    private void FixedUpdate()
    {
        if (Time.time > nextStopTime && transform.position.x > flyingArea.min.x && transform.position.x < flyingArea.max.x)
        {
            isStopping = true;
            nextStopTime = Time.time + nextStopDuration + Random.Range(minTimeBetweenStops, maxTimeBetweenStops);
        }
        if (isStopping)
        {
            LookAt(playerTransform.position);
            moleCopterShooter.CanShoot = true;

            nextStopDuration -= Time.smoothDeltaTime;
            if (nextStopDuration <= 0)
            {
                nextStopDuration = Random.Range(minStopTime, maxStopTime);
                isStopping = false;
            }
        }
        else
        {
            moleCopterShooter.CanShoot = false;
            
            // keep molecopter on z-plane 0
            Vector3 pos = transform.position;
            transform.position = new Vector3(pos.x, pos.y, 0);
        
            LookAt(currentWayPoint);
            MoveToTarget();
        }
    }
    
    void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(
            transform.position, 
            currentWayPoint, 
            Time.deltaTime * flyingSpeed);

        if (Vector3.Distance(transform.position, currentWayPoint) < 0.1f)
            SetNextWayPoint();
    }

    void SetNextWayPoint()
    {
         currentWayPoint = new Vector3(
             Random.Range(flyingArea.min.x, flyingArea.max.x),
             Random.Range(flyingArea.min.y, flyingArea.max.y),
             0
             );
    }
    
    void LookAt(Vector3 target)
    {
        // look at target, but ignore y direction
        // otherwise this leads to weird rotation of the enemy
        target.y = transform.position.y;
        transform.LookAt(target);
        
        // if molecopter is facing right, he looks into the wall instead of us
        // thus we have to rotate him
        if (target.x > transform.position.x)
            transform.Rotate(new Vector3(0f, 1f, 0f), 120f);
    }
}

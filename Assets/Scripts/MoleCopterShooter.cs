using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class MoleCopterShooter : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletSpawnPosition;
    [SerializeField] private float initialSpeed = 20;
    
    [SerializeField] private float minShootWaitTime = 0.5f, maxShootWaitTime = 2f;
    
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip bulletSound;

    private float waitTime;
    private Transform playerTransform;

    public bool CanShoot { get; set; }
    
    private void Start()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        waitTime = Time.time + Random.Range(minShootWaitTime, maxShootWaitTime);
    }

    private void Update()
    {
        if (Time.time > waitTime)
        {
            waitTime = Time.time + Random.Range(minShootWaitTime, maxShootWaitTime);
            if (CanShoot) Shoot();
        }
    }

    void Shoot()
    {
        Instantiate(bullet, bulletSpawnPosition.position, Quaternion.FromToRotation(transform.forward, playerTransform.position - transform.position));
        bullet.GetComponent<MoleCopterBullet>().ShootBullet();
        audioSource.PlayOneShot(bulletSound, 1);
    }
}

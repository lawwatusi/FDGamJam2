using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Singleton
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = FindObjectOfType<GameManager>();
            if (Instance == null)
                Instance = new GameObject().AddComponent<GameManager>();
        }
        else
        {
            if (Instance != this)
                Destroy(this);
        }
        DontDestroyOnLoad(this);
    }

    // Actual game manager
    public int EnemyCount { get; set; }
    [SerializeField] private int maxNumberOfEnemies = 30;

    public bool CanSpawnEnemies()
    {
        return EnemyCount < maxNumberOfEnemies;   
    }
}

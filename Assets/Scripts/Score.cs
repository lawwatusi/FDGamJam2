using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    
    int playerScore = 0;
    PlayerMovement  playerScript;
    GameObject scoreText;
    GameObject gameOver;
    public Text scoreTextBox;

   
    
    // Start is called before the first frame update
    void Awake()
    {
        
        scoreText = GameObject.FindWithTag("scoreUI");
        scoreTextBox = scoreText.GetComponent<Text>();
        playerScript = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
        gameOver = GameObject.FindWithTag("scoreHolder");
        gameOver.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        playerScore = playerScript.currentScore;
        scoreTextBox.text = playerScore.ToString();
    }

    public void ShowGameOver(){
        gameOver.SetActive(true);
        playerScore = playerScript.currentScore;
        scoreTextBox.text = playerScore.ToString();

    }

    
}

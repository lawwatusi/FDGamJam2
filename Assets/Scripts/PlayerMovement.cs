using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    public static PlayerMovement Instance { get; private set; }


    Rigidbody rb;
    public float moveSpeed = 1f;
    public float jumpForce;
    bool facingLeft = false;
    bool isGrounded;
    //public SpriteRenderer mySpriteRenderer;


    //Animation stuff
    private Animator anim;
    private static GameObject instance;
    public GameObject playerModel;
    public GameObject gunsModel;


    //Movement Stuff
    bool isRunning;
    bool isJumping;
    public bool controls;

    private Transform t;
    public float speed = 0.1f;
    Quaternion from;
    Quaternion to;
    private float turnTime = 0f;


    //effects
    private ParticleSystem JetSmoke;
    private ParticleSystem JetFire;


    public AudioSource audioSource;
    public AudioClip landSound;
    public AudioClip jumpSound;

    Animator animator;
    Animator shootAnimator;
    bool isRun;
    bool isShoot;

    public int currentScore;

    void Start()
    {

        animator = playerModel.GetComponent<Animator>();
        shootAnimator = gunsModel.GetComponent<Animator>();
        isRun = false;
        isShoot = false;

        //movement flip
        t = GetComponent<Transform>();
        from = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        to = Quaternion.Euler(new Vector3(0f, 90f, 0f));
        //effects
        JetSmoke = GameObject.Find("JetSmoke").GetComponent<ParticleSystem>();
        JetFire = GameObject.Find("JetFire").GetComponent<ParticleSystem>();


        controls = true;
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {
            instance = gameObject;
        }
        else
        {
            Destroy(gameObject);
        }

        //  anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        isGrounded = true;

    }

    // Update is called once per frame
    void Update()
    {
        
        // keep player on z-plane 0
        Vector3 pos = transform.position;
        transform.position = new Vector3(pos.x, pos.y, 0);

        //  gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * 100);


        if (controls)
        {
            Move();
            Jump();
        }

    }




    void Move()
    {

        float x = Input.GetAxisRaw("Horizontal");
        float moveBy = x * moveSpeed;
        rb.velocity = new Vector2(moveBy, rb.velocity.y);

        if (x == 0)
        {
            animator.SetBool("isRunning", false);
        }
        else
        {
            animator.SetBool("isRunning", true);
        }


        {

            if (Input.GetAxisRaw("Horizontal") < 0 && !facingLeft)
            {
                from = Quaternion.Euler(new Vector3(0f, 90f, 0f));
                to = Quaternion.Euler(new Vector3(0f, 270f, 0f));
                turnTime = 0f;
                Debug.Log("to: " + to.eulerAngles);
                facingLeft = true;
            }
            else if (Input.GetAxisRaw("Horizontal") > 0 && facingLeft)
            {
                from = Quaternion.Euler(new Vector3(0f, 270f, 0f));
                to = Quaternion.Euler(new Vector3(0f, 90f, 0f));
                turnTime = 0f;
                Debug.Log("to: " + to.eulerAngles);
                facingLeft = false;
            }
            turnTime += Time.deltaTime * speed;
            t.rotation = Quaternion.Lerp(from, to, turnTime);





        }



    }
    void Jump()
    {
        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //anim.SetBool("isJumping", true);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                isGrounded = false;
                audioSource.PlayOneShot(jumpSound, 0.8f);
                Debug.Log("jumping");


            }
            JetSmoke.Stop();
            JetFire.Stop();
        }
        else
        {
            JetSmoke.Play();
            JetFire.Play();
        }


    }
    void DisableMovement()
    {

        controls = false;
        Debug.Log("movement disabled");

    }
    void EnableMovement()
    {
        controls = true;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ground"))
        {
            //anim.SetBool("isJumping", false);
            isGrounded = true;
            audioSource.PlayOneShot(landSound, 0.8f);
            Debug.Log("on the ground");
            ;
        }

        // destroy mole if player jumps on it
        // we can also change this behavior to something more fancy
        if (collision.gameObject.CompareTag("MoleHead"))
        {
            Debug.Log("landed on mole head");
            Destroy(collision.transform.parent.gameObject);
            audioSource.PlayOneShot(landSound, 0.8f);
            Jump();
        }
    }
    void Die()
    {
        Debug.Log("you died!");
        this.gameObject.SetActive(false);
    }

    public float GetFacing()
    {
        if (!facingLeft) return 1;
        else return -1;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    public float damage;
    public float damageRate;
    public float pushBackForce;

    float nextDamage;

    bool playerInRange = false;

    GameObject thePlayer;
    PlayerHealth playerHealth;

    // Start is called before the first frame update
    void Start()
    {
        nextDamage = Time.time;
        thePlayer = GameObject.FindGameObjectWithTag("Player");
        playerHealth = thePlayer.GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange) Attack();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            playerInRange = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRange = false;
        }
    }
    void Attack()
    {
        if(nextDamage <= Time.time)
        {
            playerHealth.AddDamage(damage);
            nextDamage = Time.time + damageRate;

            PushBack(thePlayer.transform);
        }
    }

    void PushBack(Transform pushedObject)
    {
        Vector3 pushDirection = new Vector3((pushedObject.position.x + transform.position.x), 0, 0).normalized;
        pushDirection *= pushBackForce;

        Rigidbody pushedRB = pushedObject.GetComponent<Rigidbody>();
        pushedRB.velocity = Vector3.zero;
        //pushedRB.AddForce(pushDirection, ForceMode.Impulse);
    }
}

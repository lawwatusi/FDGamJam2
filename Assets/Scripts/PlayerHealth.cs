using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{

    public float fullHealth;
    public float currentHealth;

    public GameObject playerDeathFX;
    public GameObject damageParticles;

    public AudioSource audioSource;
    public AudioClip dieSound;

    GameObject gameOver;
    Score score;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = fullHealth;
        gameOver = GameObject.FindWithTag("PointCounter");
        score = gameOver.GetComponent<Score>();
    }
    public void AddDamage(float damage)
    {
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            MakeDead();
        }
    }

    public void damageFX(Vector3 point, Vector3 rotation)
    {
        Instantiate(damageParticles, point, Quaternion.Euler(rotation));
    }
    
    public void MakeDead()
    {
        Instantiate(playerDeathFX, transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));
        Destroy(gameObject);
        score.ShowGameOver();
    }

}

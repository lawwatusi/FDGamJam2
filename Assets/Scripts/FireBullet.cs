using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class FireBullet : MonoBehaviour
{

    public float weapon1Reload = 0.15f;
    public float weapon2Reload = 3f;
    public GameObject guns;
    public GameObject projectile1;
    public GameObject projectile2;
    public int weapon1Speed;
    public int weapon2Speed;
    

    private float nextBullet1, nextBullet2;
    public AudioSource audioSource;
    public AudioClip bulletSound;

    public GameObject gunsModel;
    Animator gunsAnimator;

    // Start is called before the first frame update
    void Awake()
    {
        gunsAnimator = gunsModel.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement playerController = transform.root.GetComponent<PlayerMovement>();
        
        if(Input.GetAxisRaw("Fire1") > 0 && nextBullet1 < Time.time)
        {
            nextBullet1 = Time.time + weapon1Reload;
           
           Instantiate(projectile1, transform.position, guns.transform.rotation);      
           projectile1.GetComponent<Rigidbody>().AddForce(guns.transform.right * weapon1Speed);
           audioSource.PlayOneShot(bulletSound, 1);

            gunsAnimator.SetBool("isShooting", true);
        }
        else if (Input.GetAxisRaw("Fire2") > 0 && nextBullet2 < Time.time)
        {
            nextBullet2 = Time.time + weapon2Reload;

            Instantiate(projectile2, transform.position, guns.transform.rotation);
            projectile2.GetComponent<Rigidbody>().AddForce(guns.transform.right * weapon2Speed);
            audioSource.PlayOneShot(bulletSound, 1);

            gunsAnimator.SetBool("isShooting", true);
        }
        else { gunsAnimator.SetBool("isShooting", false); }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBullet : MonoBehaviour
{
    public float range = 10f;
    public float damage = 5f;
    public float damageRadius = 3f;

    [SerializeField] private GameObject explosionFX;
    [SerializeField] private AudioClip explosionSound;

    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;
    LineRenderer gunLine;

    public int bulletSpeed;


    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        gunLine = GetComponent<LineRenderer>();

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;
      //  gunLine.SetPosition(0, transform.position);
        gameObject.GetComponent<Rigidbody>().AddForce(transform.forward* bulletSpeed, ForceMode.Impulse);

        //raycast hit
        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            if(shootHit.collider.tag == "Enemy")
            {
                EnemyHealth theEnemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                if(theEnemyHealth != null)
                {
                    theEnemyHealth.AddDamage(damage);
                    theEnemyHealth.damageFX(shootHit.point, -shootRay.direction);
                    Debug.Log(theEnemyHealth.currentHealth);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Damage all enemies and the player if within explosion range of missile
        if (this.CompareTag("Missile"))
        {
            Instantiate(explosionFX, transform.position, Quaternion.identity);
            SoundManager.Instance.PlaySound(explosionSound);
            // AudioSource.PlayClipAtPoint(explosionSound, transform.position, 1f);
            
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, damageRadius);
            foreach (var hitCollider in hitColliders)
            {
                // do damage according to how near / far away the enemy or player is from the explosion

                if (hitCollider.CompareTag("Enemy"))
                {
                    float weightedDamage = GetWeightedDamage(hitCollider);
                    Debug.Log("Enemy missile damage: " + weightedDamage);
                    DoEnemyDamage(hitCollider, weightedDamage);
                }
                else if (hitCollider.CompareTag("Player"))
                {
                    float weightedDamage = 0.5f * GetWeightedDamage(hitCollider);
                    Debug.Log("Player missile damage: " + weightedDamage);
                    DoPlayerDamage(hitCollider, weightedDamage);
                }
            }
        }
        
        // If bullet is not a missile and enemy is hit, just do normal damage to enemy
        else
        {
            if (other.tag == "Enemy")
                DoEnemyDamage(other, damage);
        }        
        Destroy(gameObject);
    }

    float GetWeightedDamage(Collider hitCollider)
    {
        float distance = Vector3.Distance(transform.position, hitCollider.ClosestPointOnBounds(transform.position));
        Debug.Log("distance: " + distance);
        return (1f - distance / damageRadius) * damage;
    }
    void DoEnemyDamage(Collider other, float damageAmount)
    {
        EnemyHealth theEnemyHealth = other.gameObject.GetComponent<EnemyHealth>();
        if (!theEnemyHealth) return;
        
        theEnemyHealth.AddDamage(damageAmount);
        theEnemyHealth.damageFX(other.ClosestPointOnBounds(transform.position), -GetComponent<Rigidbody>().velocity);
        Debug.Log("Enemy health: " + theEnemyHealth.currentHealth);
    }
    
    void DoPlayerDamage(Collider other, float damageAmount)
    {
        
        PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
        if (!playerHealth) return;
        
        playerHealth.AddDamage(damageAmount);
        playerHealth.damageFX(other.ClosestPointOnBounds(transform.position), -GetComponent<Rigidbody>().velocity);
        Debug.Log("Player health: " + playerHealth.currentHealth);
    }

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // Singleton
    public static SoundManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = FindObjectOfType<SoundManager>();
            if (Instance == null)
                Instance = new GameObject().AddComponent<SoundManager>();
        }
        else
        {
            if (Instance != this)
                Destroy(this);
        }
        DontDestroyOnLoad(this);
    }

    // Actual sound manager
    [SerializeField] private AudioSource audioSource;

    public void PlaySound(AudioClip clip, float volumeScale = 1f)
    {
        audioSource.PlayOneShot(clip, volumeScale);
    }
}

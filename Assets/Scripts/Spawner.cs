using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Assertions.Must;
using UnityEngine.Experimental.AI;
using UnityEngine.Serialization;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyObject;
    private Bounds playerBounds;

    private GameManager gameManager;
    
    // Level increase
    private float nextLevelIncreaseTime;
    [SerializeField] private float timeBetweenLevelIncrease =  10f;
    [SerializeField] private float decreaseTimeBetweenLevelIncreaseFactor = 0.9f;
    
    // Constant enemy stream
    private float nextEnemyTime;
    [SerializeField] private float minTimeBetweenEnemies = 3f;
    [SerializeField] private float maxTimeBetweenEnemies = 6f;

    [SerializeField] private float decreaseTimeBetweenEnemiesFactor = 0.9f;

    // Waves
    private float nextWaveTime;
    [SerializeField] private int waveSize = 2;
    [SerializeField] private float timeBetweenWaves = 10f;

    [SerializeField] private float increaseWaveSizeFactor = 1.1f;
    [SerializeField] private float decreaseTimeBetweenWavesFactor = 0.9f;
    
    // initial values for resetting
    private float initialTimeBetweenLevelIncrease;
    private float initialMinTimeBetweenEnemies, initialMaxTimeBetweenEnemies;
    private int initialWaveSize;
    private float initialTimeBetweenWaves;
    
    void initialize()
    {
        nextEnemyTime = Random.Range(0f, 3f);
        nextWaveTime = Random.Range(5f, 10f);
        nextLevelIncreaseTime = timeBetweenLevelIncrease;
    
        gameManager = GameManager.Instance;
        playerBounds = GameObject.FindWithTag("Player").GetComponent<Collider>().bounds;
    }
    
    void storeDefaults()
    {
        initialTimeBetweenLevelIncrease = timeBetweenLevelIncrease;
        
        initialMinTimeBetweenEnemies = minTimeBetweenEnemies;
        initialMaxTimeBetweenEnemies = maxTimeBetweenEnemies;

        initialWaveSize = waveSize;
        initialTimeBetweenWaves = timeBetweenWaves;
    }
    
    public void ResetValues()
    {
        timeBetweenLevelIncrease = initialTimeBetweenLevelIncrease;

        minTimeBetweenEnemies = initialMinTimeBetweenEnemies;
        maxTimeBetweenEnemies = initialMaxTimeBetweenEnemies;

        waveSize = initialWaveSize;
        timeBetweenWaves = initialTimeBetweenWaves;

        initialize();
    }

    private void Awake()
    {
        initialize();
        storeDefaults();
    }

    void Update()
    {
        // constant stream of enemies
        if (Time.time >= nextEnemyTime)
        {
            // Debug.Log("REGULAR enemy");
            StartCoroutine(Wave(1));
            nextEnemyTime += Random.Range(minTimeBetweenEnemies, maxTimeBetweenEnemies);
        }
        
        // if it's time for the next wave, spawn it
        if (Time.time >= nextWaveTime)
        {
            Debug.Log("WAVE started");
            StartCoroutine(Wave(waveSize));
            nextWaveTime += timeBetweenWaves;
        }

        // increase difficulty with time
        if (Time.time >= nextLevelIncreaseTime)
        {
            waveSize = (int)Math.Round(increaseWaveSizeFactor * waveSize);
            timeBetweenWaves *= decreaseTimeBetweenWavesFactor;
            
            minTimeBetweenEnemies *= decreaseTimeBetweenEnemiesFactor;
            maxTimeBetweenEnemies *= decreaseTimeBetweenEnemiesFactor;
            
            nextLevelIncreaseTime += timeBetweenLevelIncrease;
            timeBetweenLevelIncrease *= decreaseTimeBetweenLevelIncreaseFactor;
        }
    }

    IEnumerator Wave(int size)
    {
        for (int i = 0; i < size; i++)
        {
            if (gameManager.CanSpawnEnemies())
            {
                Instantiate(enemyObject, findSpawnPosition(), Quaternion.identity);
                gameManager.EnemyCount++;
                Debug.Log("ENEMY SPAWNED, count: " + gameManager.EnemyCount);
            }
                
            yield return new WaitForSeconds(Random.Range(minTimeBetweenEnemies, maxTimeBetweenEnemies));
        }
    }

    Vector3 findSpawnPosition()
    {
        Vector3 spawnPos;
        Bounds bounds = GetComponent<Collider>().bounds;

        // find spawn position that is not directly underneath
        // or on the side of the player
        do
        {
            spawnPos = new Vector3(
                Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),
                Random.Range(bounds.min.z, bounds.max.z)
            );
        } while ((spawnPos.x >= playerBounds.min.x && spawnPos.x <= playerBounds.max.x) ||
                 (spawnPos.y >= playerBounds.min.y && spawnPos.y <= playerBounds.max.y));

        return spawnPos;
    }
}
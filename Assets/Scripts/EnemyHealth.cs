using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{

    public float enemmyMaxHealth;
    public float damageModifier;
    public GameObject damageParticles;
    public bool drops;
    public GameObject drop;
    public AudioClip deathSound;
    public bool canBurn;
    public float burnDamage;
    public float burnTime;
    public GameObject burnEffects;
 


    bool onFire;
    float nextBurn;
    float burnInterval = 1f;
    float endBurn;

    public float currentHealth;

    public int killPoints;
    PlayerMovement playerScript;


    //public Slider enemyHealthIndicator;
    AudioSource enemyAudio;


    void Start()
    {
        currentHealth = enemmyMaxHealth;
        //enemyHealthIndicator.maxValue = enemmyMaxHealth;
        //enemyHealthIndicator.value = currentHealth;
        enemyAudio = GetComponent<AudioSource>();
        playerScript = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {

        if(currentHealth <= 0){
            MakeDead();
        }

        //if on fire
        if(onFire && Time.time > nextBurn)
        {
            AddDamage(burnDamage);
            nextBurn += burnInterval;
        }
        if(onFire && Time.time > endBurn)
        {
            onFire = false;
            burnEffects.SetActive(false);
        }
    }

    public void AddDamage(float damage)
    {
        //enemyHealthIndicator.gamePbejct.SetActive(true);
        damage = damage * damageModifier;
        if (damage <= 0) return;
        currentHealth -= damage;
        //enemyHealthIndicator.value = currentHealth;

        enemyAudio.Play();
        if (currentHealth <= 0) MakeDead();
    }

    public void damageFX(Vector3 point, Vector3 rotation)
    {
        Instantiate(damageParticles, point, Quaternion.Euler(rotation));
    }

    public void AddFire()
    {
        if (!canBurn) return;
        onFire = true;
        burnEffects.SetActive(true);
        endBurn = Time.time + burnTime;
        nextBurn = Time.time + burnInterval;
    }

    void MakeDead()
    {
        //turn off movement
        //create ragdoll
        
        SoundManager.Instance.PlaySound(deathSound);
        Destroy(gameObject);
        GameManager.Instance.EnemyCount--;
        Debug.Log("ENEMY DIED, count: " + GameManager.Instance.EnemyCount);
        playerScript.currentScore += killPoints;

        //add drops later
        //if (drops) Instantiate(drop, transform.position, transform.rotation);
    }
}
